﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Json;
using System.Text.Json;
using System.Xml;
using System.Reflection;

namespace Crafty.WindowsService
{
    public class CraftyService
    {
        private readonly ILogger<ServiceWorker> _logger;
        private readonly IHostEnvironment _hostEnvironment;
        private readonly String? _craftyDirectory;
        private readonly String? _craftyBranch;
        private readonly HttpClient _httpClient;
        private readonly JsonSerializerOptions _options = new()
        {
            PropertyNameCaseInsensitive = true
        };


        private readonly string _craftyRepositoryUrl = "https://gitlab.com/crafty-controller/";
        /* Crafty Windows Service Informations  */
        private readonly string _craftyWSVersionFile = "CraftyService/CraftyService.csproj";
        private readonly string _craftyWSName = "crafty-4-windows-service";
        /* Crafty 4 Informations  */
        private readonly string _crafty4VersionFile = "app/config/version.json";
        private readonly string _crafty4Name = "crafty-4";

        public CraftyService(HttpClient httpClient, IConfiguration configuration, ILogger<ServiceWorker> logger, IHostEnvironment hostEnvironment)
        {
            _httpClient = httpClient;
            _logger = logger;
            _hostEnvironment = hostEnvironment;

            IConfigurationSection craftyServiceConfig = configuration.GetRequiredSection("Crafty:ServiceConfig");
            IConfigurationSection userCraftyConfig = configuration.GetRequiredSection("Crafty:UserConfig");
            _craftyDirectory = craftyServiceConfig.GetValue("CraftyDirectory", _crafty4Name);
            _craftyBranch = userCraftyConfig.GetValue("CraftyBranch", "master");
        }

        public Task<CraftyWSVersion?> GetCraftyWSVersion()
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                string xmlDocumentUrl = "";
                CraftyWSVersion? version = null;

                if (_hostEnvironment.IsDevelopment())
                {
                    xmlDocumentUrl = "CraftyService.csproj";
                }
                else
                {
                    xmlDocumentUrl = GetLatestWSCraftyVersion();
                }
                xmlDocument.Load(xmlDocumentUrl);
                XmlNode? versionNode = xmlDocument.DocumentElement?.SelectSingleNode("/Project/PropertyGroup/Version");
                if ( versionNode != null)
                {
                    string[] versionArray = versionNode.InnerText.Split('.');
                    version = new CraftyWSVersion(int.Parse(versionArray[0]), int.Parse(versionArray[1]), int.Parse(versionArray[2]), 0);
                }
                return Task.FromResult(version is not null ? version : null);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error when Getting the Crafty Windows Service Version : {message}", ex.Message);
                _logger.LogDebug("Stack Trace : {stack}", ex.StackTrace);
                return Task.FromResult<CraftyWSVersion?>(null);
            }
        }
        public CraftyWSVersion? GetCraftyWSInstalledVersion()
        {
            try
            {
                CraftyWSVersion? version = null;
                // The API returns an array with a single entry.
                Version? wsVersion = Assembly.GetExecutingAssembly().GetName().Version;
                if (wsVersion != null)
                {
                    version = new CraftyWSVersion(wsVersion.Major, wsVersion.Minor, wsVersion.Build, 0);
                }
                return version is not null ? version : null;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error when Getting the Crafty Windows Service Installed Version : {message}", ex.Message);
                _logger.LogDebug("Stack Trace : {stack}", ex.StackTrace);
                return null;
            }
        }

        public async Task<CraftyVersion?> GetCraftyVersion()
        {
            try
            {
                // The API returns an array with a single entry.
                CraftyVersion? version = await _httpClient.GetFromJsonAsync<CraftyVersion>(GetLatestCraftyVersion(), _options);
                return version is not null ? version : null;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error when Getting the Crafty Version : {message}", ex.Message);
                _logger.LogDebug("Stack Trace : {stack}", ex.StackTrace);
                return null;
            }
        }
        public async Task<CraftyVersion?> GetCraftyInstalledVersion()
        {
            try
            {
                // The API returns an array with a single entry.
                using FileStream stream = File.OpenRead($"{_craftyDirectory}/{_crafty4VersionFile}");
                CraftyVersion? version = await JsonSerializer.DeserializeAsync<CraftyVersion>(stream);
                return version is not null ? version : null;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error when Getting the Crafty Installed Version : {message}", ex.Message);
                _logger.LogDebug("Stack Trace : {stack}", ex.StackTrace);
                return null;
            }
        }

        public string GetLatestWSCraftyVersion()
        {
            return $"{_craftyRepositoryUrl}{_craftyWSName}/-/raw/master/{_craftyWSVersionFile}";
        }
        public string GetCraftyWSName()
        {
            return _craftyWSName;
        }

        public string GetCraftyGitRepository()
        {
            return $"{_craftyRepositoryUrl}{_crafty4Name}.git";
        }
        public string GetLatestCraftyVersion()
        {
            return $"{_craftyRepositoryUrl}{_crafty4Name}/-/raw/{_craftyBranch}/{_crafty4VersionFile}";
        }
        public string GetCraftyName()
        {
            return _crafty4Name;
        }
        public string GetCrafty4VersionFile ()
        {
            return _crafty4VersionFile;
        }
    }

    public record CraftyWSVersion(int major, int minor, int build, int revision);
    public record CraftyVersion(int major, int minor, int sub, string meta);

    public enum Crafty_UpdateStep
    {
        ERROR_SETTING_NOT_FOUND = -4,
        ERROR_INSTALLATION = -3,
        ERROR_UPDATE = -2,
        ERROR_UNKNOWN = -1,
        NEW_INSTALL = 0,
        UPDATE_CRAFTY = 1,
        DOWNLOADED_CRAFTY = 2,
        INITED_VENV = 3,
        INSTALLED_REQUIREMENTS = 4,
        CRAFTY_READY = 5,
    }
}
