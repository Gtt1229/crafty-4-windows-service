using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Threading;

namespace Crafty.WindowsService
{
    public class ServiceWorker : BackgroundService
    {
        private readonly CraftyService _craftyService;
        private readonly ILogger<ServiceWorker> _logger;
        private readonly IHostEnvironment _hostEnvironment;
        private readonly IHostApplicationLifetime _hostApplicationLifetime;
        private readonly IConfiguration _configuration;
        private readonly IConfigurationSection _craftyConfiguration;
        private readonly IConfigurationSection _userConfiguration;
        private readonly Process _craftyProcess;
        private String? defaultPowershellPath;
        private String? defaultPyhtonPath;
        private String? defaultGitPath;
        private String? craftyDirectory;
        private String? craftyBranch;
        private bool? craftyAutoUpdate;
        private readonly CancellationTokenSource _craftyCancellation;

        public ServiceWorker(CraftyService craftyService, ILogger<ServiceWorker> logger, IHostEnvironment hostEnvironment, IHostApplicationLifetime hostApplicationLifetime, IConfiguration configuration)
        {
            _logger = logger;
            _craftyService = craftyService;
            _hostEnvironment = hostEnvironment;
            _hostApplicationLifetime = hostApplicationLifetime;
            _configuration = configuration;
            _craftyConfiguration = _configuration.GetRequiredSection("Crafty:ServiceConfig");
            _userConfiguration = _configuration.GetRequiredSection("Crafty:UserConfig");
            _craftyProcess = new Process();
            _craftyCancellation = new CancellationTokenSource();
            LoadCraftyConfig();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Crafty Service Started at: {time}", DateTimeOffset.Now);
            _logger.LogDebug("Current working directory: {workDir}", System.IO.Directory.GetCurrentDirectory());

            int exitCode = 0;

            /* Verifiying if the service as a new update */
            await CheckCraftyWSUpdate(stoppingToken);
            
            if (LoadUpdateStep() == Crafty_UpdateStep.NEW_INSTALL)
            {
                exitCode = await FirstInstallCrafty(stoppingToken);
            }
            else if (LoadUpdateStep() >= Crafty_UpdateStep.CRAFTY_READY && craftyAutoUpdate == true)
            {
                SaveUpdateStep(Crafty_UpdateStep.UPDATE_CRAFTY);
                exitCode = await UpdateCrafty(stoppingToken);
            }

            if (exitCode == 0 && !stoppingToken.IsCancellationRequested)
            {
                await RunCrafty(stoppingToken).ConfigureAwait(false);
            }

            _hostApplicationLifetime.StopApplication();
            return;
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Crafty Service is stopping.");
            if (_craftyProcess.StartInfo.FileName != "" && !_craftyProcess.HasExited)
            {
                _craftyProcess.StandardInput.WriteLine("exit");
                await _craftyProcess.WaitForExitAsync(_craftyCancellation.Token);
            }
            await base.StopAsync(cancellationToken);
            _logger.LogInformation("Crafty Service Ended at: {time}", DateTimeOffset.Now);

            return;
        }

        protected async Task<int> FirstInstallCrafty(CancellationToken stoppingToken)
        {
            int exitCode = -1;
            try
            {
                _logger.LogInformation("No Crafty Installation detected");

                if (!stoppingToken.IsCancellationRequested && LoadUpdateStep() < Crafty_UpdateStep.DOWNLOADED_CRAFTY)
                    exitCode = await DownloadCrafty(stoppingToken).ConfigureAwait(false);

                if (exitCode == 0 && !stoppingToken.IsCancellationRequested && LoadUpdateStep() < Crafty_UpdateStep.INITED_VENV)
                    exitCode = await InitVenv(stoppingToken).ConfigureAwait(false);

                if (exitCode == 0 && !stoppingToken.IsCancellationRequested && LoadUpdateStep() < Crafty_UpdateStep.INSTALLED_REQUIREMENTS)
                    exitCode = await InitCrafty(stoppingToken).ConfigureAwait(false);

                if (exitCode == 0)
                {
                    SaveUpdateStep(Crafty_UpdateStep.CRAFTY_READY);
                    _logger.LogInformation("Crafty Successfully Installed, Ready To Start");
                }
                else
                {
                    _logger.LogError("Issue Detected when trying to Install Crafty");
                }
            }
            catch
            {
                _logger.LogError("Issue Detected when trying to Install Crafty");
            }
            return exitCode;
        }

        protected async Task<int> UpdateCrafty(CancellationToken stoppingToken)
        {
            bool updateAvailable = false;
            int exitCode = -1;
            try
            {
                _logger.LogInformation("Crafty Installation detected in : {craftyDirectory}", craftyDirectory);

                if (!stoppingToken.IsCancellationRequested)
                    updateAvailable = await CheckCraftyUpdate(stoppingToken).ConfigureAwait(false);

                if (!stoppingToken.IsCancellationRequested && updateAvailable)
                {
                    if (!_craftyConfiguration.GetValue("AutoUpdate", false))
                    {
                        _logger.LogWarning("An Update is Available, but Crafty AutoUpdate is disabled.");
                    }
                    else
                    {
                        _logger.LogInformation("Crafty Updating ...");
                        exitCode = await UpdateCraftyV2(stoppingToken).ConfigureAwait(false);
                        _logger.LogInformation("Crafty Updated");

                        if (exitCode == 0 && !stoppingToken.IsCancellationRequested && LoadUpdateStep() < Crafty_UpdateStep.INSTALLED_REQUIREMENTS)
                            exitCode = await InitCrafty(stoppingToken).ConfigureAwait(false);

                        _logger.LogInformation("Crafty Requirements Updated");
                    }
                }
                else
                {
                    SaveUpdateStep(Crafty_UpdateStep.CRAFTY_READY);
                    _logger.LogInformation("No need to update Crafty !");
                    return exitCode=0;
                }

                if (exitCode == 0)
                {
                    SaveUpdateStep(Crafty_UpdateStep.CRAFTY_READY);
                    _logger.LogInformation("Crafty Successfully Updated, Ready To Start");
                }
                else
                {
                    _logger.LogError("Issue Detected when trying to Update Crafty");
                }

            }
            catch
            {
                _logger.LogError("Issue Detected when trying to Update Crafty ");
            }
            return exitCode;
        }

        protected async Task<bool> CheckCraftyUpdate(CancellationToken stoppingToken)
        {
            bool retour = false;

            if (stoppingToken.IsCancellationRequested)
                return false;

            CraftyVersion? version = await _craftyService.GetCraftyVersion();
            if (version == null)
            {
                _logger.LogError("Can't reach the Crafty Version info");
            }
            else
            {
                _logger.LogInformation("Latest Available Crafty Version {major}.{minor}.{sub}", version.major, version.minor, version.sub);
            }

            CraftyVersion? installedVersion = await _craftyService.GetCraftyInstalledVersion();
            if (installedVersion == null)
            {
                _logger.LogError("No Crafty version found");
            }
            else
            {
                _logger.LogInformation("Installed Version {major}.{minor}.{sub}", installedVersion.major, installedVersion.minor, installedVersion.sub);
            }

            if ((version == null) || (installedVersion == null))
            {
                return retour;
            }

            if ((version.major > installedVersion.major) ||
                (version.major == installedVersion.major && version.minor > installedVersion.minor) ||
                (version.major == installedVersion.major && version.minor == installedVersion.minor) && (version.sub > installedVersion.sub))
            {
                retour = true;
            }

            return retour;
        }
        protected async Task<bool> CheckCraftyWSUpdate(CancellationToken stoppingToken)
        {
            bool retour = false;

            if (stoppingToken.IsCancellationRequested)
                return false;

            CraftyWSVersion? version = await _craftyService.GetCraftyWSVersion();
            if (version == null)
            {
                _logger.LogError("Can't reach the Crafty Windows Service Version info");
            }
            else
            {
                _logger.LogInformation("Latest Available Crafty Windows Service Version {major}.{minor}.{build}", version.major, version.minor, version.build);
            }

            CraftyWSVersion? installedVersion = _craftyService.GetCraftyWSInstalledVersion();
            if (installedVersion == null)
            {
                _logger.LogError("No Crafty Windows Service version found");
            }
            else
            {
                _logger.LogInformation("Installed Version {major}.{minor}.{sub}", installedVersion.major, installedVersion.minor, installedVersion.build);
            }

            if ((version == null) || (installedVersion == null))
            {
                return retour;
            }

            if ((version.major > installedVersion.major) ||
                (version.major == installedVersion.major && version.minor > installedVersion.minor) ||
                (version.major == installedVersion.major && version.minor == installedVersion.minor) && (version.build > installedVersion.build))
            {
                retour = true;
            }

            return retour;
        }

        protected async Task<int> DownloadCrafty(CancellationToken stoppingToken)
        {
            if (LoadUpdateStep() > Crafty_UpdateStep.DOWNLOADED_CRAFTY)
            {
                _logger.LogInformation("Crafty Already Dowloaded, going to next step");
                return 0;
            }

            _logger.LogInformation("Downloading Crafty Started");

            int lastExitCode = 0;
            string GitRipository = _craftyService.GetCraftyGitRepository();

            Process cmd = new Process();
            cmd.StartInfo.FileName = defaultGitPath + @"\git.exe";
            cmd.StartInfo.Arguments = $"clone -q {GitRipository}";
            cmd.StartInfo.RedirectStandardInput = false;
            // Set our event handler to asynchronously read the sort output.
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.OutputDataReceived += ConsoleOutputHandler;
            cmd.StartInfo.RedirectStandardError = true;
            cmd.ErrorDataReceived += ConsoleErrorHandler;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.BeginOutputReadLine();
            cmd.BeginErrorReadLine();

            await cmd.WaitForExitAsync(stoppingToken);
            lastExitCode = cmd.ExitCode;

            IConfigurationSection craftyDirectoryTemp = _craftyConfiguration.GetRequiredSection("CraftyDirectory");
            craftyDirectoryTemp.Value = _craftyService.GetCraftyName();

            if (_hostEnvironment.IsDevelopment())
            {
                craftyDirectoryTemp.Value = _craftyService.GetCraftyName();
            }
            _configuration.GetReloadToken();
            LoadCraftyConfig();

            if (lastExitCode == 0)
            {
                _logger.LogInformation("Downloading Crafty Finished");
                SaveUpdateStep(Crafty_UpdateStep.DOWNLOADED_CRAFTY);
            }
            else
            {
                _logger.LogError("Downloading Crafty Failed");
            }
            cmd.Dispose();
            return lastExitCode;
        }

        protected async Task<int> UpdateCraftyV2(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Downloading last version of Crafty Started");

            if (LoadUpdateStep() > Crafty_UpdateStep.DOWNLOADED_CRAFTY)
            {
                _logger.LogInformation("Crafty Already Dowloaded, going to next step");
                return 0;
            }

            int lastExitCode = 0;
            string GitRipository = _craftyService.GetCraftyGitRepository();

            Process cmd = new Process();
            cmd.StartInfo.FileName = defaultPowershellPath + @"\powershell.exe";
            cmd.StartInfo.WorkingDirectory = $"{craftyDirectory}/..";
            cmd.StartInfo.Arguments = $"git clone -q {GitRipository} {_craftyService.GetCraftyName() + "_new"}; " +
                                      $"cd {_craftyService.GetCraftyName() + "_new"}; git checkout {craftyBranch}; cd ..; " +
                                      $"Remove-Item {craftyDirectory + "_old"} -Recurse -Force;" +
                                      $"Copy-Item -Path {craftyDirectory} -Destination {craftyDirectory + "_old"} -Recurse -Force; " +
                                      $"Copy-Item -Path {craftyDirectory + "_new"}\\* -Destination {craftyDirectory} -Recurse -Force; " +
                                      $"Remove-Item {craftyDirectory + "_new"} -Recurse -Force";
            cmd.StartInfo.RedirectStandardInput = false;
            // Set our event handler to asynchronously read the sort output.
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.OutputDataReceived += ConsoleOutputHandler;
            cmd.StartInfo.RedirectStandardError = true;
            cmd.ErrorDataReceived += ConsoleErrorHandler;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.BeginOutputReadLine();
            cmd.BeginErrorReadLine();

            await cmd.WaitForExitAsync(stoppingToken);
            lastExitCode = cmd.ExitCode;
            if (lastExitCode == 0)
            {
                _logger.LogInformation("Downloading last version of Crafty Finished");
                SaveUpdateStep(Crafty_UpdateStep.DOWNLOADED_CRAFTY);
            }
            else
            {
                _logger.LogError("Downloading last version of Crafty Failed");
            }
            cmd.Dispose();
            return lastExitCode;
        }

        protected async Task<int> InitVenv(CancellationToken stoppingToken)
        {
            if (LoadUpdateStep() > Crafty_UpdateStep.INITED_VENV)
            {
                _logger.LogInformation("Python venv Already Initialized, going to next step");
                return 0;
            }

            _logger.LogInformation("Python venv Initializing ...");

            int lastExitCode = 0;

            Process cmd = new Process();
            cmd.StartInfo.FileName = defaultPyhtonPath + @"\python";
            cmd.StartInfo.WorkingDirectory = $"{craftyDirectory}";
            cmd.StartInfo.Arguments = "-m venv venv";
            cmd.StartInfo.RedirectStandardInput = false;
            // Set our event handler to asynchronously read the sort output.
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.OutputDataReceived += ConsoleOutputHandler;
            cmd.StartInfo.RedirectStandardError = true;
            cmd.ErrorDataReceived += ConsoleErrorHandler;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.BeginOutputReadLine();
            cmd.BeginErrorReadLine();

            await cmd.WaitForExitAsync(stoppingToken);
            lastExitCode = cmd.ExitCode;

            if (lastExitCode == 0)
            {
                _logger.LogInformation("Python venv Initialized");
                SaveUpdateStep(Crafty_UpdateStep.INITED_VENV);
            }
            else
            {
                _logger.LogError("Python venv Initialization Failed");
            }
            cmd.Dispose();
            return lastExitCode;
        }

        protected async Task<int> InitCrafty(CancellationToken stoppingToken)
        {
            if (LoadUpdateStep() > Crafty_UpdateStep.INSTALLED_REQUIREMENTS)
            {
                _logger.LogInformation("Crafty Already Initialized, going to next step");
                return 0;
            }

            _logger.LogInformation("Crafty Initializing ...");

            int lastExitCode = 0;

            Process cmd = new Process();
            cmd.StartInfo.FileName = defaultPowershellPath + @"\powershell.exe";
            cmd.StartInfo.WorkingDirectory = $"{craftyDirectory}";
            cmd.StartInfo.Arguments = $"Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process; \"./venv/Scripts/Activate.ps1\"; python -m pip install -r requirements.txt";
            cmd.StartInfo.RedirectStandardInput = false;
            // Set our event handler to asynchronously read the sort output.
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.OutputDataReceived += ConsoleOutputHandler;
            cmd.StartInfo.RedirectStandardError = true;
            cmd.ErrorDataReceived += ConsoleErrorHandler;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.BeginOutputReadLine();
            cmd.BeginErrorReadLine();

            await cmd.WaitForExitAsync(stoppingToken);
            lastExitCode = cmd.ExitCode;

            if (lastExitCode == 0)
            {
                _logger.LogInformation("Crafty Initialized");
                SaveUpdateStep(Crafty_UpdateStep.INSTALLED_REQUIREMENTS);
            }
            else
            {
                _logger.LogError("Crafty Initialization Failed");
            }
            cmd.Dispose();
            return lastExitCode;
        }

        protected async Task RunCrafty(CancellationToken stoppingToken)
        {
            if (LoadUpdateStep() < Crafty_UpdateStep.CRAFTY_READY)
            {
                _logger.LogWarning("Crafty not ready to start : missing steps before starting it, trying to solve it.");
                if (0 != await FirstInstallCrafty(stoppingToken).ConfigureAwait(false))
                    return;
            }

            _logger.LogInformation("Crafty�Process Starting...");

            _craftyCancellation.TryReset();

            _craftyProcess.StartInfo.FileName = defaultPowershellPath + @"\powershell.exe";
            _craftyProcess.StartInfo.WorkingDirectory = craftyDirectory;
            if (_hostEnvironment.IsDevelopment())
            {
                _craftyProcess.StartInfo.Arguments = $"cd \"{System.IO.Directory.GetCurrentDirectory()}/{craftyDirectory}/\"; \"./venv/Scripts/Activate.ps1\"; python main.py";
            }
            else if (_hostEnvironment.IsProduction())
            {
                _craftyProcess.StartInfo.Arguments = $"Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process; \"./venv/Scripts/Activate.ps1\"; python main.py -v";
            }
            _craftyProcess.StartInfo.RedirectStandardInput = true;
            // Set our event handler to asynchronously read the sort output.
            _craftyProcess.StartInfo.RedirectStandardOutput = true;
            _craftyProcess.OutputDataReceived += ConsoleOutputHandler;
            _craftyProcess.StartInfo.RedirectStandardError = true;
            _craftyProcess.ErrorDataReceived += ConsoleErrorHandler;
            _craftyProcess.StartInfo.CreateNoWindow = true;
            _craftyProcess.StartInfo.UseShellExecute = false;
            _craftyProcess.Start();
            _craftyProcess.BeginOutputReadLine();
            _craftyProcess.BeginErrorReadLine();

            await _craftyProcess.WaitForExitAsync(CancellationToken.None);
            if (_craftyProcess.ExitCode == 0)
            {
                _logger.LogInformation("Crafty Process Succefully Stopped");
                _logger.LogInformation("Crafty Process Terminated at: {time}", DateTimeOffset.Now);
            }
            else
            {
                _logger.LogError("Crafty Process Exit Code : {exitCode}", _craftyProcess.ExitCode);
                _logger.LogError("Crafty Process Terminated at: {time}", DateTimeOffset.Now);
            }

            _craftyCancellation.Cancel();
            _craftyCancellation.Dispose();

            return;
        }

        protected void ConsoleOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            // Collect the sort command output.
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                if (outLine.Data.Contains("CRITICAL"))
                {
                    _logger.LogCritical("{data}", outLine.Data);
                }
                else if (outLine.Data.Contains("ERROR"))
                {
                    _logger.LogError("{data}", outLine.Data);
                }
                else if (outLine.Data.Contains("WARNING"))
                {
                    _logger.LogWarning("{data}", outLine.Data);
                }
                else if (outLine.Data.Contains("INFO"))
                {
                    _logger.LogInformation("{data}", outLine.Data);
                }
                else if (outLine.Data.Contains("DEBUG"))
                {
                    _logger.LogDebug("{data}", outLine.Data);
                }
                else if (outLine.Data.Contains("TRACE"))
                {
                    _logger.LogTrace("{data}", outLine.Data);
                }
                else
                {
                    _logger.LogDebug("{data}", outLine.Data);
                }
            }
        }
        protected void ConsoleErrorHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            // Collect the sort command output.
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                _logger.LogError("{data}", outLine.Data);
            }
        }

        protected void LoadCraftyConfig()
        {
            defaultPowershellPath = @"C:\Windows\System32\WindowsPowerShell\v1.0";
            defaultPyhtonPath = _craftyConfiguration.GetValue("DefaultPythonPath", "");
            defaultGitPath = _craftyConfiguration.GetValue("DefaultGitPath", "");
            craftyDirectory = _craftyConfiguration.GetValue("CraftyDirectory", _craftyService.GetCraftyName());
            craftyAutoUpdate = _craftyConfiguration.GetValue("AutoUpdate", false);
            craftyBranch = _userConfiguration.GetValue("CraftyBranch", "master");

            if (_hostEnvironment.IsProduction())
            {
                System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            }
        }
        protected Crafty_UpdateStep LoadUpdateStep()
        {
            return Enum.Parse<Crafty_UpdateStep>(_craftyConfiguration.GetValue("UpdateStep", "ERROR_SETTING_NOT_FOUND"));
        }
        protected void SaveUpdateStep(Crafty_UpdateStep step)
        {
            _craftyConfiguration["UpdateStep"] = step.ToString();

            try
            {
                var filePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "appSettings.json");
                string jsonString = File.ReadAllText(filePath);
                JsonNode craftySettingNode = JsonNode.Parse(jsonString)!;

                craftySettingNode!["Crafty"]!["ServiceConfig"]!["UpdateStep"] = step.ToString();

                var options = new JsonSerializerOptions { WriteIndented = true };
                string output = craftySettingNode.ToJsonString(options);
                File.WriteAllText(filePath, output);
            }
            catch
            {
                Console.WriteLine("Error writing app settings");
            }
        }
    }
}