using Crafty.WindowsService;

IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService(options =>
    {
        options.ServiceName = "CraftyService";
    })
    .ConfigureServices(services =>
    {
        services.AddHostedService<ServiceWorker>();
        services.AddHttpClient<CraftyService>();
    })
    .ConfigureLogging(logging =>
    {
        logging.AddEventLog(eventlog =>
        {
            eventlog.SourceName = "CraftyService";
        });
        logging.AddSimpleConsole(options =>
        {
            options.IncludeScopes = true;
            options.SingleLine = true;
            options.TimestampFormat = "yyyy/MM/dd - hh:mm:ss ";
            options.UseUtcTimestamp = true;
            options.ColorBehavior = Microsoft.Extensions.Logging.Console.LoggerColorBehavior.Enabled;
        });
        
    })
    .Build();

await host.RunAsync();
