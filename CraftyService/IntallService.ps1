Write-Host "****************************************************************************" -ForegroundColor Green
Write-Host "*                                                                          *" -ForegroundColor Green
Write-Host "* This tool will Install a Service to update and launch Crafty on Windows. *" -ForegroundColor Green
Write-Host "*                                                                          *" -ForegroundColor Green
Write-Host "****************************************************************************" -ForegroundColor Green

#Check if it runs as an Admin for Service Installation
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
  Write-Host ""
  Write-Host "You must run this script as an Administrator" -ForegroundColor DarkRed
  Write-Host "Installer is now quitting..."
}
else {
    #Define Parameters
    $serviceName = "CraftyService"
    $startupType = "Manual"
    $userName = "CraftyUser"
    $userDesc = "The user account to lauch Crafty"

    Write-Host ""
    Write-Host "The Logging parameters and the Crafty parameters can be set in appsettings.json" -ForegroundColor Blue

    Write-Host "Creation of the Crafty User to run Crafty" -ForegroundColor DarkBlue
    #$user = Get-LocalUser -Name $userName
    $user = Get-LocalUser | Where-Object {$_.Name -eq "$userName"}
    if ($null -eq $user) {
        # Create the Crafty Service
        Write-Host "Crafty User doesn't exists, creating this user" -ForegroundColor DarkBlue
        $pass = Read-Host "Define a password for the Crafty User" -AsSecureString
        New-LocalUser -Name $userName -Description $userDesc -Password $pass -PasswordNeverExpires -AccountNeverExpires -UserMayNotChangePassword
    }
    else {
        Write-Host "Crafty User already exists, skipping this step" -ForegroundColor DarkBlue
    }

    # Give RXW permissions on the folder
    Write-Host "Giving access permissions at the CraftyUser to the folder containing the Service" -ForegroundColor DarkBlue
    $user = Get-LocalUser | Where-Object {$_.Name -eq "$userName"}
    $servicePath = $PSScriptRoot
    icacls $PSScriptRoot --% /grant:r CraftyUser:(OI)(CI)(RX,W) /T /Q

    # Create the Crafty Service
    Write-Host "Creation of the Crafty Service to run Crafty" -ForegroundColor DarkBlue
    $service = Get-WmiObject -Class Win32_Service -Filter "Name='$serviceName'"
    if ($null -eq $service) {
    Write-Host "Service Crafty is Installing" -ForegroundColor Blue
    $autoStart = Read-Host -Prompt 'Do you want to launch the Service at Computer startup ? [Y] = Yes, [N] = no'

    if (($autoStart -eq "Y") -or ($autoStart -eq "y")) {
        $startupType = "Automatic"
    }

    $params = @{
        Name           = $serviceName
        BinaryPathName = $PSScriptRoot + "\CraftyService.exe"
        DisplayName    = "Crafty Service"
        StartupType    = $startupType
        Description    = "This is a test service for Crafty."
    }
    New-Service @params

    #Changing the Service User
    #sc.exe config $serviceName obj= "NT AUTHORITY\LocalService"
    sc.exe config $serviceName obj= ".\CraftyUser"
    }
    else
    {
        Write-Host "The Crafty Service is already installed, no need to install it." -ForegroundColor DarkGreen
        Write-Host "Installation Canceled"
    }

    if ($null -eq $pass) {
        $pass = Read-Host "Give the password of the Crafty User" -AsSecureString
    }
    #$credential = New-Object System.Management.Automation.PSCredential ($userName, $pass)
    #$process = Start-Process Powershell -ArgumentList '.\checkInstallation.ps1' -Credential $credential -PassThru -Wait -NoNewWindow
    #$process.WaitForExit()

    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.FileName = "powershell"
    $pinfo.RedirectStandardError = $true
    $pinfo.RedirectStandardOutput = $true
    $pinfo.UseShellExecute = $false
    $pinfo.UserName = $userName
    $pinfo.Password = $pass
    $pinfo.WorkingDirectory = $PSScriptRoot
    $pinfo.CreateNoWindow = $true
    $pinfo.Arguments = ".\checkInstallation.ps1"
    $p = New-Object System.Diagnostics.Process
    $p.StartInfo = $pinfo
    $p.Start() | Out-Null
    $stdout = $p.StandardOutput.ReadToEnd()
    $p.WaitForExit()
    if ($p.ExitCode -eq 0)
    {
        Write-Host "$stdout" -ForegroundColor Green
    }
    else
    {
        Write-Host "$stdout" -ForegroundColor Red
    }
}