Write-Host "****************************************************************************" -ForegroundColor Green
Write-Host "*                                                                          *" -ForegroundColor Green
Write-Host "* This tool will Uninstall the Crafty Service                              *" -ForegroundColor Green
Write-Host "*                                                                          *" -ForegroundColor Green
Write-Host "****************************************************************************" -ForegroundColor Green

#Check if it runs as an Admin for Service Installation
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write-Host ""
    Write-Host "You must run this script as an Administrator" -ForegroundColor DarkRed
    Write-Host "Installer is now quitting..."
}
else {
    Write-Host ""
    #Define Parameters
    $serviceName = "CraftyService"
    $userName = "CraftyUser"
    $service = Get-WmiObject -Class Win32_Service -Filter "Name='$serviceName'"
    if ($null -eq $service) {
        Write-Host "The Crafty Service is not installed, no need to uninstall it." -ForegroundColor DarkGreen
        Write-Host "Uninstallation Canceled"
    }
    else {
        Write-Host "Service Crafty is Uninstalling" -ForegroundColor Blue
        Write-Host "Are you sure you want to uninstall the Crafty Service ?" -ForegroundColor Red
        $confirmation = Read-Host -Prompt '[Y] = Yes, [N] = no'

        if (($confirmation -eq "Y") -or ($confirmation -eq "y")) {
            Write-Host "Uninstallation ..."
            $service.delete()

            # Give RXW permissions on the folder
            Write-Host "Removing access permissions at the CraftyUser to the folder containing the Service" -ForegroundColor DarkBlue
            $user = Get-LocalUser | Where-Object {$_.Name -eq "$userName"}
            $servicePath = $PSScriptRoot
            icacls $PSScriptRoot --% /remove:g CraftyUser /Q

            Write-Host "Do you want to remove the CraftyUSer ?" -ForegroundColor Red
            $confirmation = Read-Host -Prompt '[Y] = Yes, [N] = no'            
            if (($confirmation -eq "Y") -or ($confirmation -eq "y")) {
                Write-Host "Remove of the Crafty User to run Crafty" -ForegroundColor DarkBlue
                Remove-LocalUser -Name $userName
                Write-Host "Crafty User Removed" -ForegroundColor DarkRed
            }
            else {
                Write-Host "CraftyUser Not Removed"  -ForegroundColor DarkBlue
            }
        }
        else {
            Write-Host "Uninstallation Canceled"
        }
    }
}