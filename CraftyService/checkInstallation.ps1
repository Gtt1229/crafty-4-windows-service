#Define Parameters
$userName = "CraftyUser"

#Checking if CraftyUser is used
$currentUser = (whoami).Split('\')[1]
if ($currentUser -ne $userName) {
    Write-Host "CraftyUser didn't run this script" -ForegroundColor Red
    Write-Host "I can't verify if CraftyUser has access to needed programs" -ForegroundColor Red
}
else {
    $exitCode = 0
    #Checking .Net Installation
    try {
        $dotnetVersion = dotnet --version
        Write-Host "CraftyUser can run .Net $dotnetVersion" -ForegroundColor Green
    }
    catch
    {
        $exitCode += 1
        Write-Host "CraftyUser doesn't have access to .Net, is it installed ?" -ForegroundColor Red
    }
    #Checking Git Installation
    try {
        $gitVersion = git --version
        Write-Host "CraftyUser can run $gitVersion" -ForegroundColor Green
    }
    catch
    {
        $exitCode += 2
        Write-Host "CraftyUser doesn't have access to Git, is it installed ?" -ForegroundColor Red
    }
    #Checking Python Installation
    try {
        $pythonVersion = python --version
        Write-Host "CraftyUser can run $pythonVersion" -ForegroundColor Green
    }
    catch
    {
        $exitCode += 4
        Write-Host "CraftyUser doesn't have access to Python, is it installed ?" -ForegroundColor Red
    }
}