[![Crafty Logo](CraftyService/images/logo_long.svg)](https://craftycontrol.com)

# Windows Service for Crafty Controller
> .Net6 based Windows Service to Run Crafty as a service on Windows

## What is Crafty Controller?
Crafty Controller is a Minecraft Server Control Panel / Launcher. The purpose
of Crafty Controller is to launch a Minecraft Server in the background and present
a web interface for the server administrators to interact with their servers. Crafty
is compatible with Docker, Linux, Windows 7, Windows 8 and Windows 10.

## Documentation
Documentation available on [Crafty's Docs](https://docs.craftycontrol.com/pages/getting-started/installation/windows/#crafty-windows-service-beta)

## Meta
Crafty Project Homepage - https://craftycontrol.com

Discord Server - https://discord.gg/9VJPhCE

Git Repository - https://gitlab.com/crafty-controller/crafty-4-windows-service


# Installation on Windows 10/11
## Requirements
- Windows 10 or 11
- Python 3.10 or higher, installed at the computer level (for all users)
- [.NET 6](https://dotnet.microsoft.com/download/dotnet/6.0) or Higher
- Internet Connection to download Crafty Files

## Content of the package
- IntallService.ps1 : Powershell Script used to Install the Service on the computer
- UninstallService.ps1 : Powershell Script used to Uninstall the service on the computer
- appsettings.json : Configuration file to modifiy some services settings (Installation Folder, Auto-Update, ...)
- CraftyService.exe : The executable of the service (The one that does all the work)

## Installation Steps
### Preparing with Installation Script
1. Start the powershell console with administrator privileges
2. Run the `InstallService.ps1` script
3. Firstly, it will create a user to run the service and asks for a password for this user.
4. Then, it will asks if you want to start the service at Startup
5. And thats' it for the Installation Script
6. The powershell console can be closed
### Now we need to be sure that the service can start
1. Open the services manager.
2. Go to the Crafty Service properties.
3. In the connexion tab, check if Crafty is well used for opening a session (you need to fill again the password you set earlier)
4. Then you can Start the Crafty Service


# Installation on Windows Server
## Requirements
- Windows 2016 or Higher
- Python 3.10 or higher, installed at the computer level (for all users)
- [.NET 6](https://dotnet.microsoft.com/download/dotnet/6.0) or Higher
- Internet Connection to download Crafty Files

## Content of the package
- IntallService.ps1 : Powershell Script used to Install the Service on the computer
- UninstallService.ps1 : Powershell Script used to Uninstall the service on the computer
- appsettings.json : Configuration file to modifiy some services settings (Installation Folder, Auto-Update, ...)
- CraftyService.exe : The executable of the service (The one that does all the work)

## Installation Steps
### Preparing with Installation Script
1. Start the powershell console with administrator privileges
2. Run the `InstallService.ps1` script
3. Firstly, it will create a user to run the service and asks for a password for this user.
4. Then, it will asks if you want to start the service at Startup
5. And thats' it for the Installation Script
6. The powershell console can be closed
### Now we need to be sure that the service can start
1. Open the services manager.
2. Go to the Crafty Service properties.
3. In the connexion tab, check if Crafty is well used for opening a session (you need to fill again the password you set earlier)

### We need to grant the CraftyUser to run services in the Windows Group Policy
> This step is not needed if the precedent step give you a dialog box of adding the CraftyUser to it.
1. Open the Group Policy Strategy Editor
2. Go in Computer Config
3. Then in Windows Settings
4. Then in Security Settings
5. Then in Local Strategies
6. Then in User rights attribution
7. You need to add the Crafty User at the `Open a session as a service`
### Now Crafty Service is ready to run
1. Go in the Services manager
2. Then you can Start the Crafty Service

# Updating Crafty Service
## Requirements
- Rights on the folder containing the Crafty Service Files
- Downloads the latests realease of the Crafty Service
## Update Steps
1. Stop the Crafty Service in the Service Manager
2. Copy/Paste, the new files in place of the old ones
3. Start again the Crafty Service in the Service Manager

# Uninstalling the Service
> :warning: Don't forget to save your Minecraft Worlds before uninstalling
## Uninstallation Steps
1. Start the powershell console with administrator privileges
2. Run the `UninstallService.ps1` script
3. Firstly, it will remove the service from windows.
4. Then, it will asks if you want to remove the CraftyUser
5. And thats' it for the Uninstallation Script
6. The powershell console can be closed
7. You can now delete your server files.

# Configuring the Service
In the Folder you have a configuration file named `appsettings.json`
The part you want to use is this one :
```json
"Crafty": {
    "ServiceConfig": {
      "DefaultGitPath": "C:\\Program Files\\Git\\bin",
      "DefaultPythonPath": "C:\\Program Files\\Python3",
      "CraftyDirectory": "crafty-4",
      "AutoUpdate": true,
      "UpdateStep": "NEW_INSTALL"
    },
    "UserConfig": {
      "CraftyBranch":  "master"
    }
  }
```

- If you installed Git in a different folder that the default one, you can set its path in the `DefaultGitPath` field.
- If you installed Python in a different folder or have a different vertsion that the default one, you can set its path in the `DefaultPythonPath` field.
- If you want to place Crafty in a different Directory that the Service Files, tou can set its path in the `CraftyDirectory` field. *Not recommended due to Access Rights*
- You can configure if you want Crafty to be updated at the Service Startup in `AutoUpdate` field.
- If you want to be with a specific branch of Crafty, you can set it in the `CraftyBranch` field. *(Works only with master or dev branch for now.)*
- The `UpdateStep` is here to know if the installation crashed while performing and at which step. You have to don't touch it.

